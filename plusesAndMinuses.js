// When a user clicks the + element, the count should increase by 1 on screen.
// When a user clicks the – element, the count should decrease by 1 on screen.
let counter =0;
document.getElementById('result').innerHTML = counter;
const plusEl = document.getElementById('buttonPlus');
plusEl.addEventListener('click', (e) => {
    counter++;
    document.getElementById('result').innerHTML = counter;
});

const minusEl = document.getElementById('buttonMinus');
minusEl.addEventListener('click', (e) => {
    counter--;
    document.getElementById('result').innerHTML = counter;
});