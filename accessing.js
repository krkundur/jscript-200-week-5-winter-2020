// Change the text of the "Seattle Weather" header to "February 10 Weather Forecast, Seattle"

// Change the styling of every element with class "sun" to set the color to "orange"

// Change the class of the second <li> from to "sun" to "cloudy"
const weather_link = document.getElementById('weather-head');
weather_link.innerHTML = "February 10 Weather Forecast, Seattle";
const sun_class = document.getElementsByClassName('sun');
Array.from(sun_class).forEach((element) => {
    element.style.color = 'orange';
})
sun_class[0].className = 'cloudy';
const secondLi = document.getElementsByTagName('li')[1];
secondLi.className = 'cloudy';