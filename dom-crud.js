// Create a new <a> element containing the text "Buy Now!" 
// with an id of "cta" after the last <p>
const elementA = document.createElement('a');
const elementNode = document.createTextNode('Buy Now');
elementA.appendChild(elementNode);
const para = document.querySelector('main');
para.appendChild(elementA);

// Access (read) the data-color attribute of the <img>,
// log to the console

const img = document.getElementsByTagName('img')[0];
console.log(img.dataset.color);

// Update the third <li> item ("Turbocharged"), 
// set the class name to "highlight"

const li3 = document.getElementsByTagName('li')[2];
li3.className = 'highlight';

// Remove (delete) the last paragraph
// (starts with "Available for purchase now…")

const pElements = document.getElementsByTagName('p');
const p = pElements[pElements.length - 1];
para.removeChild(p);